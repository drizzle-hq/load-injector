package org.bitbucket.loadinjector

import scala.concurrent.duration._
import scala.util.{Failure, Success}
import akka.actor.{Actor, ActorRef, Props}

object VirtualUser {

  case object ProcessScript

  def props(
      resultCollector: ActorRef,
      script: Script,
      initialDelay: Duration,
      pacing: Duration
    ) = Props(new VirtualUser(resultCollector, script, initialDelay, pacing))
}

class VirtualUser(resultCollector: ActorRef, script: Script, initialDelay: Duration, pacing: Duration) extends Actor {
  import context.dispatcher
  import VirtualUser._

  val start = System.nanoTime()

  override def preStart() = {
    initialDelay match {
      case finiteInitialDelay: FiniteDuration =>
        scheduleNext(finiteInitialDelay)
      case _ =>
    }
  }

  override def receive = {
    case ProcessScript =>
      scheduleNext(pacing)
      process(script)
  }

  private def scheduleNext(time: Duration) = time match {
    case finiteTime: FiniteDuration =>
      context.system.scheduler.scheduleOnce(finiteTime, self, ProcessScript)
    case _ =>
  }

  private def process(script: Script) = {
    val scriptStart = System.nanoTime()
    val relativeStart = Duration.fromNanos(scriptStart - start)
    def elapsedTime = Duration.fromNanos(System.nanoTime() - scriptStart)

    try {
      script() andThen {
        case Success(_) => resultCollector ! ScriptSuccessful(relativeStart, elapsedTime)
        case Failure(t) => resultCollector ! ScriptFailed(relativeStart, elapsedTime, t)
      }
    } catch {
      case t: Throwable => resultCollector ! ScriptFailed(relativeStart, elapsedTime, t)
    }    
  }
}