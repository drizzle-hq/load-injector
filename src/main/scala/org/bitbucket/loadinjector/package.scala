package org.bitbucket

import scala.concurrent.Future
import scala.concurrent.duration.FiniteDuration

package object loadinjector {

  type Script = () => Future[Any]

  sealed abstract class ScriptResult(startTime: FiniteDuration, elapsedTime: FiniteDuration)
  case class ScriptSuccessful(startTime: FiniteDuration, elapsedTime: FiniteDuration) extends ScriptResult(startTime, elapsedTime)
  case class ScriptFailed(startTime: FiniteDuration, elapsedTime: FiniteDuration, reason: Throwable) extends ScriptResult(startTime, elapsedTime)

}