package org.bitbucket.loadinjector

import scala.concurrent.duration.FiniteDuration
import java.io.PrintStream
import akka.actor.{Actor, Props}

object ResultCollector {

  def props(output: PrintStream) = Props(new ResultCollector(output))
}

class ResultCollector(output: PrintStream) extends Actor {

  override def preStart() = {
    output.append("start,elapsed,error\n")
  }

  override def postRestart(reason: Throwable) = {
    // override so that it does not call preStart
  }

  override def receive() = {
    case ScriptSuccessful(startTime, elapsedTime) => print(startTime, elapsedTime, "")
    case ScriptFailed(startTime, elapsedTime, failure) => print(startTime, elapsedTime, failure.getMessage)
  }

  private def print(startTime: FiniteDuration, elapsedTime: FiniteDuration, failureMessage: String) = {
    output.append(s"${startTime.toMillis},${elapsedTime.toMillis},${failureMessage}\n")
  }
}
