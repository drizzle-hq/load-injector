package integration

import scala.concurrent.Future
import scala.concurrent.duration._
import akka.actor.ActorSystem
import akka.testkit.TestKit
import akka.testkit.TestProbe
import org.scalatest.{BeforeAndAfterAll, FlatSpecLike, MustMatchers}
import org.bitbucket.loadinjector._

class VirtualUserSpec extends TestKit(ActorSystem("Test"))
  with FlatSpecLike
  with MustMatchers
  with BeforeAndAfterAll {

  import system.dispatcher

  val testTimeout = 2 seconds

  val tolerance = 30 millis

  def emptyScript = () => Future {}

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "A Virtual User" should "process the script immediately if no delay is set" in {
    var counter = 0

    val probe = TestProbe()
    val virtualUser = system.actorOf(VirtualUser.props(probe.ref, () => Future { counter += 1 }, Duration.Zero, Duration.Undefined))

    probe.expectMsgType[ScriptSuccessful](tolerance)

    counter must be(1)
  }

  it should "process the script after the delay if set" in {
    val probe = TestProbe()
    val delay = 200 millis

    val virtualUser = system.actorOf(VirtualUser.props(probe.ref, emptyScript, delay, Duration.Undefined))

    probe.expectNoMsg(delay - tolerance)
    probe.expectMsgType[ScriptSuccessful](2 * tolerance)
  }

  it should "not process the script if undefined delay is passed" in {
    val probe = TestProbe()

    val virtualUser = system.actorOf(VirtualUser.props(probe.ref, emptyScript, Duration.Undefined, Duration.Undefined))

    probe.expectNoMsg(tolerance)
  }

  it should "process the script at a certain pace" in {
    val probe = TestProbe()
    val pacing = 50 millis

    val virtualUser = system.actorOf(VirtualUser.props(probe.ref, emptyScript, Duration.Zero, pacing))

    probe.receiveN(4, 4 * pacing + tolerance)
  }

  it should "process the script only once if undefined pacing is passed" in {
    val probe = TestProbe()

    val virtualUser = system.actorOf(VirtualUser.props(probe.ref, emptyScript, Duration.Zero, Duration.Undefined))

    probe.expectMsgType[ScriptSuccessful](tolerance)
    probe.expectNoMsg(tolerance)
  }

  it should "return a failure if the script throws" in {
    val probe = TestProbe()

    val virtualUser = system.actorOf(VirtualUser.props(probe.ref, () => Future { throw new Exception }, Duration.Zero, Duration.Undefined))

    probe.expectMsgType[ScriptFailed](tolerance)
  }
  
  it should "return a failure if the script throws before returning the future" in {
    val probe = TestProbe()

    val virtualUser = system.actorOf(VirtualUser.props(probe.ref, () => throw new Exception, Duration.Zero, Duration.Undefined))

    probe.expectMsgType[ScriptFailed](tolerance)
  }

  it should "return timings for the script" in {
    val probe = TestProbe()
    val scriptTime = 50 millis
    val timingTolerance = 2 millis

    val virtualUser = system.actorOf(VirtualUser.props(probe.ref, () => Future { Thread.sleep(scriptTime.toMillis) }, Duration.Zero, Duration.Undefined))

    probe.receiveOne(tolerance + scriptTime) match {
      case ScriptSuccessful(_, measuredTime) =>
        measuredTime must (be >= scriptTime and be <= scriptTime + timingTolerance)
    }
  }

  it should "return the relative start time for the script" in {
    val probe = TestProbe()
    val delay = 200 millis

    val virtualUser = system.actorOf(VirtualUser.props(probe.ref, emptyScript, delay, Duration.Undefined))

    probe.receiveOne(delay + tolerance) match {
      case ScriptSuccessful(startTime, _) =>
        startTime must (be >= delay and be <= delay + tolerance)
    }
  }

  it should "process scripts returning values" in {
    val probe = TestProbe()
    def scriptReturningAValue(): Future[Int] = Future(42)

    val virtualUser = system.actorOf(VirtualUser.props(probe.ref, scriptReturningAValue, Duration.Zero, Duration.Undefined))

    probe.expectMsgType[ScriptSuccessful](tolerance)
  }

}
